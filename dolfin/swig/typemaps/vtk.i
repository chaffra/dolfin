#ifdef HAS_VTK
%{
#include "vtkPythonUtil.h"
%}
%typemap(out) vtkSmartPointer<vtkActor2D> {

PyImport_ImportModule("vtk");

$result = vtkPythonUtil::GetObjectFromPointer ( (vtkSmartPointer<vtkActor2D>)$1 );

}

%typemap(in) vtkSmartPointer<vtkActor2D> {

/*$1 = NULL;*/

$1 = (vtkSmartPointer<vtkActor2D>) vtkPythonUtil::GetPointerFromObject ( $input, "vtkActor2D" );

if ( $1 == NULL ) { SWIG_fail; }

}
#endif