#ifndef __DOLFIN_PLOT_H
#define __DOLFIN_PLOT_H

// DOLFIN plot interface

#include <dolfin/plot/plot.h>
#include <dolfin/plot/VTKPlotter.h>
#include <dolfin/plot/GenericVTKPlottable.h>
#include <dolfin/plot/VTKPlottableMesh.h>
#include <dolfin/plot/VTKPlottableGenericFunction.h>
#include <dolfin/plot/VTKPlottableGenericFunction1D.h>
#include <dolfin/plot/ExpressionWrapper.h>

#endif
