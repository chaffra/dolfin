// Copyright (C) 2012 Joachim B Haga.
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2012-09-11
// Last changed: 2012-09-13

#ifndef __VTK_PLOTTABLE_GENERICFUNCTION_1D_H
#define __VTK_PLOTTABLE_GENERICFUNCTION_1D_H

#ifdef HAS_VTK

#include <memory>
#include <vtkSmartPointer.h>

#include <dolfin/mesh/Mesh.h>
#include "GenericVTKPlottable.h"
#include "VTKPlottableMesh.h"
#include "VTKPlottableGenericFunction.h"

class vtkXYPlotActor;

namespace dolfin
{

  class Mesh;
  class VTKWindowOutputStage;

  ///

  class VTKPlottableGenericFunction1D : public GenericVTKPlottable
  {
  public:

    explicit VTKPlottableGenericFunction1D();

    virtual ~VTKPlottableGenericFunction1D();

    std::shared_ptr<VTKPlottableGenericFunction> get_plottable(std::shared_ptr<const Variable> var);

    //--- Implementation of the GenericVTKPlottable interface ---

    /// Additional parameters for VTKPlottableGenericFunction1D
    virtual void modify_default_parameters(Parameters& p)
    {
      p["scalarbar"] = false;
      p["lc"] = "1.0,0.0,0.0"; //line color
      p.add("logx", false);
      p.add("logy", false);
    }

    /// Initialize the parts of the pipeline that this class controls
    virtual void init_pipeline(const Parameters &parameters);

    /// Connect or reconnect to the output stage.
    virtual void connect_to_output(VTKWindowOutputStage& output);

    /// Update the plottable data
    virtual void update(std::shared_ptr<const Variable> var,
                        const Parameters& p, int frame_counter);

    /// Inform the plottable about the range.
    virtual void rescale(double range[2], const Parameters& p);

    /// Return whether this plottable is compatible with the variable
    virtual bool is_compatible(const Variable& var) const;

    /// Update the scalar range of the plottable data
    virtual void update_range(double range[2]);

    /// Return geometric dimension
    virtual std::size_t dim() const {return 1;};

    /// Get the actor
    virtual vtkSmartPointer<vtkActor2D> get_actor2D();

    /// Get an actor for showing vertex labels
    virtual vtkSmartPointer<vtkActor2D> get_vertex_label_actor(vtkSmartPointer<vtkRenderer>);

    /// Get an actor for showing cell labels
    virtual vtkSmartPointer<vtkActor2D> get_cell_label_actor(vtkSmartPointer<vtkRenderer>);

    virtual void set_logx(int val);

    virtual void set_logy(int val);

  private:

    vtkSmartPointer<vtkXYPlotActor> _actor;

    std::vector<std::shared_ptr<VTKPlottableGenericFunction> > _xy_plottables;
  };

}

#endif

#endif
