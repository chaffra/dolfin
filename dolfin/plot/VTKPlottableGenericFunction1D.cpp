// Copyright (C) 2012 Joachim B Haga
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2012-09-11
// Last changed: 2012-09-17

#ifdef HAS_VTK

#include <vtkXYPlotActor.h>
#include <vtkUnstructuredGrid.h>
#include <vtkProperty2D.h>
#include <vtkGlyphSource2D.h>
#include <vtkTextProperty.h>
#include <vtkDataSetCollection.h>
#include <vtkAxisActor2D.h>

#include <dolfin/common/Timer.h>
#include <dolfin/common/constants.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/FunctionSpace.h>

#include "ExpressionWrapper.h"
#include "VTKWindowOutputStage.h"
#include "VTKPlottableGenericFunction1D.h"

#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>

#ifdef foreach
#undef foreach
#endif
#define foreach BOOST_FOREACH

using namespace dolfin;

//----------------------------------------------------------------------------
VTKPlottableGenericFunction1D::VTKPlottableGenericFunction1D()
  : _actor(vtkSmartPointer<vtkXYPlotActor>::New())
{
  dolfin_assert(dim() == 1);
  // Do nothing
}
VTKPlottableGenericFunction1D::~VTKPlottableGenericFunction1D(){
	//_actor->Delete();
}
//----------------------------------------------------------------------------
std::shared_ptr<VTKPlottableGenericFunction> VTKPlottableGenericFunction1D::get_plottable(
		std::shared_ptr<const Variable> var) {

	std::shared_ptr<const Function> function(
			std::dynamic_pointer_cast<const Function>(var));
	std::shared_ptr<const ExpressionWrapper> wrapper(
			std::dynamic_pointer_cast<const ExpressionWrapper>(var));

	dolfin_assert(function || wrapper);
	std::shared_ptr<VTKPlottableGenericFunction> plottable;

    foreach(plottable, _xy_plottables){

    	if (function)
    	{
    		//info("Trying to get stored plottable function");
    		//info("stored plottable function is used %d times", plottable->_function.use_count() );
    		if(plottable->_function->id() == function->id()) {
    			log(TRACE,"Found 1");
    			return plottable;
    		}
    	}
    	else if (wrapper)
    	{
    		//info("Trying to get stored plottable expression");
    		log(TRACE, "stored plottable expression is used %d times", plottable->_function.use_count() );
    		log(TRACE, "wapper expression id is  %d", wrapper->expression()->id() );
    		if (plottable->_function->id() == wrapper->expression()->id()){
    			log(TRACE,"Found 1");
    			return plottable;
    		}
    		else {
    			log(TRACE, "None found!");
    		}
    	}


    }
    //info("Done");

    //info("Adding new plottable");
	if (function) {
		plottable.reset(new VTKPlottableGenericFunction(function));
		//info("stored plottable function is used %d times", plottable->_function.use_count() );
	} else if (wrapper) {
		plottable.reset(
				new VTKPlottableGenericFunction(wrapper->expression(),
						wrapper->mesh()));
		//info("stored plottable expression is used %d times", plottable->_function.use_count() );

	}
	//info("Done");
	#if VTK_MAJOR_VERSION <= 5
	_actor->AddInput(plottable->grid());
	#else
	_actor->AddDataSetInput(plottable->grid());
  	#endif
	_xy_plottables.push_back(plottable);
	return plottable;

}

//----------------------------------------------------------------------------
void VTKPlottableGenericFunction1D::init_pipeline(const Parameters& p)
{
  //VTKPlottableGenericFunction::init_pipeline(p);
  //_actor->AddInput(grid());

  _actor->SetXValuesToValue();

  _actor->GetProperty()->SetColor(0.0, 0.0, 0.8);
  _actor->GetProperty()->SetLineWidth(1.5);
  _actor->GetProperty()->SetPointSize(4);
  //_actor->PlotPointsOn();
  //_actor->SetPlotPoints(0, 1);
  _actor->PlotPointsOff();

  std::string xlabel = (std::string)p["xlabel"];
  _actor->SetXTitle(xlabel.c_str());
  std::string ylabel = (std::string)p["ylabel"];
  _actor->SetYTitle(ylabel.c_str());

  _actor->GetYAxisActor2D()->GetLabelTextProperty()->SetOrientation(90.0);

  _actor->GetAxisLabelTextProperty()->ShadowOff();
  _actor->GetAxisLabelTextProperty()->ItalicOff();
  _actor->GetAxisLabelTextProperty()->SetColor(0, 0, 0);

  _actor->GetAxisTitleTextProperty()->ShadowOff();
  _actor->GetAxisTitleTextProperty()->ItalicOff();
  _actor->GetAxisTitleTextProperty()->SetColor(0, 0, 0);

  _actor->GetPositionCoordinate ()->SetValue(0, 0, 1);
  _actor->GetPosition2Coordinate()->SetValue(1, 1, 0);
  _actor->SetBorder(30);

  #if (VTK_MAJOR_VERSION == 6) || ((VTK_MAJOR_VERSION == 5) && (VTK_MINOR_VERSION >= 6))
  _actor->SetReferenceYValue(0.0);
  _actor->ShowReferenceXLineOff();
  #endif
  _actor->SetAdjustYLabels(false); // Use the ranges set in rescale()
}
//----------------------------------------------------------------------------
void VTKPlottableGenericFunction1D::connect_to_output(VTKWindowOutputStage& output)
{
  output.add_viewprop(_actor);
}
//----------------------------------------------------------------------------
bool VTKPlottableGenericFunction1D::is_compatible(const Variable &var) const
{
  const GenericFunction *function(dynamic_cast<const Function*>(&var));
  const ExpressionWrapper *wrapper(dynamic_cast<const ExpressionWrapper*>(&var));
  const Mesh *mesh(NULL);

  if (function)
  {
    mesh = static_cast<const Function*>(function)->function_space()->mesh().get();
  }
  else if (wrapper)
  {
    mesh = wrapper->mesh().get();
  }

  if (!mesh || mesh->topology().dim() != 1)
  {
    return false;
  }

  return true; //VTKPlottableGenericFunction::is_compatible(var);
}
//----------------------------------------------------------------------------
void VTKPlottableGenericFunction1D::update_range(double range[2])
{
  // Superclass gets the range from the grid
	range[0] = 0.0;
	range[1] = 0.0;
	std::shared_ptr<dolfin::VTKPlottableGenericFunction> plottable;
	foreach(plottable, _xy_plottables){
		double prange[2];
		plottable->update_range(prange);
		range[0] = std::min(prange[0],range[0]);
		range[1] = std::max(prange[1],range[1]);
	}
}
//----------------------------------------------------------------------------
void VTKPlottableGenericFunction1D::update(std::shared_ptr<const Variable> var,
                                           const Parameters& p, int framecounter)
{
	std::shared_ptr<dolfin::VTKPlottableGenericFunction> plottable;
	log(TRACE, "Updating 1D plot.");
	if (var)
	{
		plottable = get_plottable(var);

		dolfin_assert(plottable->dim() == 1);
		plottable->init_pipeline(p);
		plottable->update(var, p, framecounter);

		int i = 0;
		while (i < _xy_plottables.size()) {

			if (_xy_plottables[i]->grid().GetPointer()
					== plottable->grid().GetPointer()) {

				std::vector<std::string> colors;
				std::string color = (std::string) p["lc"];
				boost::split(colors, color, boost::is_any_of(","),
						boost::token_compress_on);
				double r = ::atof(colors[0].c_str());
				double g = ::atof(colors[1].c_str());
				double b = ::atof(colors[2].c_str());
				_actor->SetPlotColor(i, r, g, b);
				break;

			}
			i++;
		}
	}
	else
	{
		foreach(plottable, _xy_plottables) {
			plottable->update(var,p,framecounter);
		}

	}

	std::string xlabel = (std::string) p["xlabel"];
	_actor->SetXTitle(xlabel.c_str());
	std::string ylabel = (std::string) p["ylabel"];
	_actor->SetYTitle(ylabel.c_str());



	//double* bounds = grid()->GetBounds(); // [xmin xmax ymin ymax zmin zmax]
	//_actor->SetXRange(bounds);
}
//----------------------------------------------------------------------------
void VTKPlottableGenericFunction1D::rescale(double range[2],
                                            const Parameters& p)
{
	_actor->SetYRange(range);
//  #if (VTK_MAJOR_VERSION == 5) && (VTK_MINOR_VERSION >= 6)
//  if (range[0] < 0 && range[1] > 0)
//    _actor->ShowReferenceYLineOn();
//  else
//    _actor->ShowReferenceYLineOff();
//  #endif
}
//----------------------------------------------------------------------------
vtkSmartPointer<vtkActor2D> VTKPlottableGenericFunction1D::get_actor2D()
{
	return _actor;
}
//----------------------------------------------------------------------------
vtkSmartPointer<vtkActor2D> VTKPlottableGenericFunction1D::get_vertex_label_actor(vtkSmartPointer<vtkRenderer> renderer)
{
  return GenericVTKPlottable::get_vertex_label_actor(renderer);
}
//----------------------------------------------------------------------------
vtkSmartPointer<vtkActor2D> VTKPlottableGenericFunction1D::get_cell_label_actor(vtkSmartPointer<vtkRenderer> renderer)
{
  return GenericVTKPlottable::get_cell_label_actor(renderer);
}
//----------------------------------------------------------------------------
void VTKPlottableGenericFunction1D::set_logx(int val){
	_actor->SetLogx(val);
}
//----------------------------------------------------------------------------
void VTKPlottableGenericFunction1D::set_logy(int val){
	_actor->SetLogy(val);
//	double range[2];
//	_actor->GetYRange(range);
//	_actor->SetYRange(range);
}
#endif
